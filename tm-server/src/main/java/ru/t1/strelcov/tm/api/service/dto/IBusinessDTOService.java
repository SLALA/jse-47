package ru.t1.strelcov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.AbstractBusinessEntityDTO;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.List;

public interface IBusinessDTOService<E extends AbstractBusinessEntityDTO> extends IDTOService<E> {

    @NotNull
    E add(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    E updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    E updateByName(@Nullable String userId, @Nullable String oldName, @Nullable String name, @Nullable String description);

    @NotNull
    E changeStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    E changeStatusByName(@Nullable String userId, @Nullable String oldName, @NotNull Status status);

    @NotNull
    List<E> findAll(@Nullable final String userId);

    @NotNull
    List<E> findAll(@Nullable final String userId, @Nullable final String sort);

    @NotNull
    E findByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    E removeByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    E findById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    E removeById(@Nullable final String userId, @Nullable final String id);

    void clear(@Nullable final String userId);

}
