package ru.t1.strelcov.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.ITokenService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;

import java.util.Optional;

@Setter
public abstract class AbstractCommand implements ITokenService {

    @NotNull
    protected ServiceLocator serviceLocator;

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @NotNull final String name = name();
        final String arg = arg();
        final String description = description();
        if (!name.isEmpty()) result += name + " ";
        if (arg != null && !arg.isEmpty()) result += "[" + arg + "] ";
        if (!description.isEmpty()) result += "- " + description;
        return result;
    }

    @NotNull
    @Override
    public String getToken() {
        @Nullable final String token = serviceLocator.getTokenService().getToken();
        Optional.ofNullable(token).filter((i) -> !i.isEmpty()).orElseThrow(AccessDeniedException::new);
        return token;
    }

    @Override
    public void setToken(@NotNull final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

}
