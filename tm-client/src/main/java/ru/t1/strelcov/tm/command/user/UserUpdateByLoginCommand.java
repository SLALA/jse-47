package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.UserUpdateByLoginRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-update-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Update user info by login.";
    }

    @Override
    public void execute() {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        System.out.println("[UPDATE USER INFO]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserDTO user = userEndpoint.updateUserByLogin(new UserUpdateByLoginRequest(getToken(), login, firstName, lastName, email)).getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
