package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.dto.request.TaskRemoveByNameRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        taskEndpoint.removeByNameTask(new TaskRemoveByNameRequest(getToken(), name));
    }

}
