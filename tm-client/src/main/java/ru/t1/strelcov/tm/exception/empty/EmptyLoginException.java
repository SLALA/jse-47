package ru.t1.strelcov.tm.exception.empty;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error: Login is empty.");
    }

}
